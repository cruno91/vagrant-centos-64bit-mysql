This is a base box image to be used by Vagrant.
CentOS 6.5 x86_64 with MySQL 5.7


Other installed packages:
- wget
- gcc
- kernel-devel
- bzip2
- git
- vim
- curl
- ntp
- man
- ssh

***
Installation:

1. `git clone git@gitlab.com:kroono/vagrant-centos-64bit-mysql.git`
2. `cd vagrant-centos-64bit-mysql`
3. `vagrant box add centos65-64bit-mysql75 centos65-64bit-mysql75.box`

***
You are now ready to start a new project or use the box in an existing project


Start a new project:
`vagrant init centos65-64bit-mysql57`

***
Credentials:

Login - root/vagrant

MySQL - root/root

***
TODO:
Utilize tool for better versioning than just relying on git branches
